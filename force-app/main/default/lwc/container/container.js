import { LightningElement, track, api } from 'lwc';

class entry{
    constructor(id, type, value, editable){
        this.id = id;
        this.type = type;
        this.value = value;
        this.editable = editable;
    }
}
export default class App extends LightningElement {  
    @api
        addrow(id){
            console.log('adding row ' + id);
            this.rows.push({id: id, fields: []});
        }
    @api
        addtorow(id, type, value, editable){
            console.log('adding to row... ' + id + ' ' + type + ' ' + value + ' ' + editable);
            this.rows.find(
                row => row.id == id
            ).fields.push(new entry(this.column, type, value, editable));

            this.column++;
        }
    @api
        modifyColumn(rowid, type, newValue){

        }
    @api
        headers;
    @track
        rows = [];
    @api
        candelete;
    id = 0;
    column = 0;
    handleEdit(event){
        console.log('handling edit');
        console.log(JSON.stringify(event.detail));
        const row = this.rows.find(
            row => row.id == event.detail.row
        );
        const cell = row.fields.find(cell => cell.id == event.detail.column);
        cell.value = event.detail.fieldvalue;
        console.log(cell);
        //send event
        const saveEvent = new CustomEvent('save',{
            detail: row
        });
        this.dispatchEvent(saveEvent);
    }
    removeCriteria(event){
        console.log('hey delete this');
        console.log(event.target.name);
        const row = this.rows.findIndex(
            row => row.id == event.target.name
        );
        this.rows.splice(row, 1);
        //send event
        const deleteEvent = new CustomEvent('delete',{
            detail:{
                id: event.target.name
            }
        });
        this.dispatchEvent(deleteEvent);
    }
}
