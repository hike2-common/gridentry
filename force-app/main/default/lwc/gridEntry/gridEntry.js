import { LightningElement, api, wire } from 'lwc';
import { deleteRecord } from 'lightning/uiRecordApi';
import { updateRecord } from 'lightning/uiRecordApi';
import { createRecord } from  'lightning/uiRecordApi';
import getLineItems from '@salesforce/apex/gridEntryController.getLineItems';

const TYPES = [];
export default class GridEntry extends LightningElement {
    headers = [{title : 'Product', id: 0}, {title : 'Annual Units', id: 1}, {title: 'Special Pricing', id: 2}, {title: 'Description', id: 3}, {title: 'Dealer Net', id: 4}, {title: 'Discount Percentage', id: 5}];
    allowdelete = true;
    id = 0;
    @api recordId;

    @wire(getLineItems, {recordId: '$recordId'})
        currentCase({error, data}){
            if(error){
                console.log('error');
                console.log(this.recordId);
                console.log(error);
            }
            else if(data){
                /*
                    Pre-populate data table with existing line items
                */
                console.log('data');
                data.forEach(
                    lineItem => {
                        console.log('adding item');
                        console.log(lineItem);
                        const container = this.template.querySelector('c-container');
                        container.addrow(lineItem.Id);
                        container.addtorow(lineItem.Id, 'Product', lineItem.Product__c, false);
                        container.addtorow(lineItem.Id, 'Annual Units', lineItem.Annual_Units__c ?  lineItem.Annual_Units__c : 1, true);
                        container.addtorow(lineItem.Id, 'Special Pricing', lineItem.Special_Price__c ? lineItem.Special_Price__c : 0, true);
                        container.addtorow(lineItem.Id, 'Description', lineItem.SPR_Description__c ? lineItem.SPR_Description__c : ' ', false);
                        container.addtorow(lineItem.Id, 'Dealer Net', lineItem.SPR_Dealer_Net_Price__c ? lineItem.SPR_Dealer_Net_Price__c : 'Unavailable', false);
                        container.addtorow(lineItem.Id, 'Discount Percentage', lineItem.SPR_Discount_Percentage__c ? lineItem.SPR_Discount_Percentage__c : 'Unavailable', false);
                    }
                );
                console.log('added data');
            }
        }
    handleSave(event){
        /*
            update line item
        */
        console.log('saving...');
        console.log(event.detail.id);
        console.log('fields are:');
        console.log(JSON.stringify(event.detail.fields));
        const fields = {};
        fields['Id'] = event.detail.id;
        fields['Special_Price__c'] = event.detail.fields[2].value;
        fields['Annual_Units__c'] = event.detail.fields[1].value;
        console.log(fields);
        const recordInput = {fields};
        updateRecord(recordInput)
            .then(() => {
                //update field values in data table & update discount % field
            })
            .catch(error => {

            });
    }
    handleDelete(event){
        /*
            delete line item
        */
        console.log('deleting...');
        console.log(event.detail.id);
        deleteRecord(event.detail.id)
            .then(()=>{

            })
            .catch(error =>{

            });
    }
    handleClick(event){
        /*
            create new line item
        */
        console.log('clicking');
        const fields = {};
        fields['Product__c'] = '01t2E00000NvQV8QAN';
        fields['SPR_Special_Pricing_Request__c'] = this.recordId;
        const recordInput = { apiName: 'Special_Pricing_Request_Line_Item__c', fields};
        console.log(recordInput);
        createRecord(recordInput)
            .then(lineItem =>{
                console.log(lineItem)
                console.log(lineItem.id);
                console.log(lineItem.fields);
                console.log(lineItem.fields.SPR_Description__c.value);
                const container = this.template.querySelector('c-container');
                container.addrow(lineItem.id);
                container.addtorow(lineItem.id, 'Product', lineItem.fields.Product__r.displayValue, false);
                container.addtorow(lineItem.id, 'Annual Units', lineItem.fields.Annual_Units__c.value ?  lineItem.Annual_Units__c : 1, true);
                container.addtorow(lineItem.id, 'Special Pricing', lineItem.fields.Special_Price__c.value ? lineItem.Special_Price__c : 0, true);
                container.addtorow(lineItem.id, 'Description', lineItem.fields.SPR_Description__c.value ? lineItem.fields.SPR_Description__c.value : ' ', false);
                container.addtorow(lineItem.id, 'Dealer Net', lineItem.fields.SPR_Dealer_Net_Price__c.value ? lineItem.fields.SPR_Dealer_Net_Price__c.value : 'Unavailable', false);
                container.addtorow(lineItem.id, 'Discount Percentage', lineItem.fields.SPR_Discount_Percentage__c.value ? lineItem.fields.SPR_Discount_Percentage__c.value : 'Unavailable', false);
            })
            .catch(error =>{
                console.log(error);
            });
        this.id++;
    }
}