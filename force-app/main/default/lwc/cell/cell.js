import { LightningElement, api, track } from 'lwc';

export default class Cell extends LightningElement {
    @api fieldvalue;
    @api column;
    @api editable;
    @api title;
    @api row;
    @track
        trackedvalue;
    @track
    editing = false;

    handleEdit(event){
        this.editing = true;
        this.trackedvalue = this.fieldvalue;
    }
    handleSave(event){
        this.editing = false;
        this.fieldvalue = this.trackedvalue;
        const saveEvent = new CustomEvent('edit',{
            detail:{
                column: this.column,
                fieldvalue: this.trackedvalue,
                row: this.row
            }
        });
        this.dispatchEvent(saveEvent);
    }
    handleInput(event){
        this.trackedvalue = event.target.value;
    }
}