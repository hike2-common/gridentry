public with sharing class gridEntryController {
    @AuraEnabled(cacheable=true)
    public static List<Special_Pricing_Request_Line_Item__c> getLineItems(Id recordId) {
        return [SELECT Id, Annual_Units__c, SPR_Dealer_Net_Price__c, SPR_Description__c, Product__c, Special_Price__c, SPR_Discount_Percentage__c
                    FROM Special_Pricing_Request_Line_Item__c
                    WHERE SPR_Special_Pricing_Request__c = :recordId];
    }
}
